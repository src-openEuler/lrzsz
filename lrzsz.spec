%global debug_package %{nil}

Name:          lrzsz
Version:       0.12.20
Release:       47
License:       GPLv2+
Summary:       Free x/y/zmodem implementation
Url:           http://www.ohse.de/uwe/software/lrzsz.html
Source:        http://www.ohse.de/uwe/releases/%{name}-%{version}.tar.gz

Patch0001:     lrzsz-0.12.20-glibc21.patch
Patch0002:     lrzsz-0.12.20.patch
Patch0003:     lrzsz-0.12.20-man.patch
Patch0004:     lrzsz-0.12.20-aarch64.patch
BuildRequires: gcc gettext

%description
lrzsz is a unix communication package providing the XMODEM,
YMODEM ZMODEM file transfer protocols. lrzsz is a heavily
rehacked version of the last public domain release of Omen
Technologies rzsz package.

%prep
%autosetup -p1

rm -f po/*.gmo

%build
%configure --disable-pubdir --enable-syslog --program-transform-name=s/l//
%make_build LDFLAGS="%{__global_ldflags} -s"

%install
%makeinstall
%find_lang %{name}

%files -f %{name}.lang
%doc COPYING README
%{_bindir}/*
%{_mandir}/*/*

%changelog
* Mon Aug 28 2023 xu_ping <707078654@qq.com> - 0.12.20-47
- add strip

* Tue Nov 5 2019 Yiru Wang <wangyiru1@huawei.com> - 0.12.20-46
- Pakcage init
